module Main
    where

{-
  From a list of numbers, find the SINGLE pair that sums 
  to 2020 and output the product of the pair.
-}

import Data.List

stringToInt x = read x :: Integer
toPairs list = [[x, y] | (x:ys) <- tails list, y <- ys]
toTriplets list = [[x, y, z] | [x, y] <- toPairs list, (z:_) <- tails list ]
isDesiredPair :: [Integer] -> Bool
isDesiredPair pair = sum pair == 2020
pairProduct :: Num a => (a, a) -> a 
pairProduct (x, y) = x * y

findProduct transformer = getProduct . transformData
    where transformData = transformer . map stringToInt . words
          getProduct = head . map product . filter isDesiredPair

main :: IO()
main = do
    input <- readFile "input.txt"
    let tupleOutput = findProduct toPairs input
    let tripletOutput = findProduct toTriplets input
    print ("Tuple: " ++ show tupleOutput ++ ", Triplet: " ++ show tripletOutput )
